package hopeclient

import (
	"errors"
	"strconv"
	"time"

	"strings"

	"sort"

	"github.com/extemporalgenome/slug"
	"github.com/jeffail/gabs"
	"github.com/parnurzeal/gorequest"
)

type Client interface {
	SlugToItemId(slug string) *int

	SlugToGroupId(slug string) *int

	ShippingMethods() []*ShippingMethod

	Version() *int

	UnitKinds() *UnitKindMap

	Item(itemId int) *Item

	Items(groupId int) []*Item

	ItemsAll() []*Item

	Dosages(itemId int) []string

	Pack(packId int) *Pack

	Packs(itemId int) []*Pack

	PacksForDosage(itemId int, dosage string) []*Pack

	MinUnitPricePack(itemId int) *Pack

	Assets(itemId int) []*Asset

	Group(groupId int) *Group

	Groups() []*Group

	Search(what string) []*Item
}

type Catalog struct {
	loaded time.Time
	data   *gabs.Container
	url    string

	// Will be prepolulated when catalog loaded, so we will
	// have ability to quick resolve item or group slug to id.
	itemSlugs  map[string]int
	groupSlugs map[string]int
}

func New(url string) (*Catalog, error) {
	resp, body, errs := gorequest.New().Get(url).End()
	if len(errs) > 0 {
		return nil, errs[0]
	}
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	data, err := gabs.ParseJSON([]byte(body))
	if err != nil {
		return nil, err
	}

	catalog := &Catalog{
		loaded:     time.Now().UTC(),
		data:       data,
		url:        url,
		itemSlugs:  make(map[string]int, 0),
		groupSlugs: make(map[string]int, 0),
	}

	// Calculate slugs for items
	items, _ := catalog.data.S("items").Children()
	for _, child := range items {
		id := int(child.S("id").Data().(float64))
		slug := slug.Slug(child.S("name").Data().(string))
		catalog.itemSlugs[slug] = id
	}

	// Calculate slugs for groups
	groups, _ := catalog.data.S("groups").Children()
	for _, child := range groups {
		id := int(child.S("id").Data().(float64))
		slug := slug.Slug(child.S("name").Data().(string))
		catalog.groupSlugs[slug] = id
	}

	return catalog, nil
}

func (c *Catalog) SlugToItemId(slug string) *int {
	if itemId, exist := c.itemSlugs[slug]; exist {
		return &itemId
	}
	return nil
}

func (c *Catalog) SlugToGroupId(slug string) *int {
	if groupId, exist := c.groupSlugs[slug]; exist {
		return &groupId
	}
	return nil
}

func (c *Catalog) ShippingMethods() []*ShippingMethod {
	methods := make([]*ShippingMethod, 0)
	items, _ := c.data.S("shipping").Children()
	for _, child := range items {
		m := ShippingMethod{}
		m.Id = int(child.S("id").Data().(float64))
		freeThreshold, _ := strconv.ParseFloat(child.S("free_threshold").Data().(string), 64)
		m.FreeThreshold = freeThreshold
		price, _ := strconv.ParseFloat(child.S("price").Data().(string), 64)
		m.Price = price
		m.Desc = child.S("desc").String()
		m.Name = child.S("name").String()
		methods = append(methods, &m)
	}
	return methods
}

func (c *Catalog) Version() *int {
	version := int(c.data.S("meta", "schema_version").Data().(float64))
	return &version
}

func (c *Catalog) UnitKinds() *UnitKindMap {
	kinds := UnitKindMap{}
	items := c.data.S("meta", "unit_kinds").Data().(map[string]interface{})
	for k, v := range items {
		kinds[k] = v.(string)
	}
	return &kinds
}

func (c *Catalog) Item(itemId int) *Item {
	items, _ := c.data.S("items").Children()
	for _, child := range items {
		id := int(child.S("id").Data().(float64))
		if id == itemId {
			item := Item{}
			item.Id = id
			item.Name = child.S("name").Data().(string)
			item.Desc = child.S("desc").Data().(string)
			item.Slug = slug.Slug(item.Name)
			groups := make([]int, 0)
			items2, _ := child.S("groups").Children()
			for _, child2 := range items2 {
				groups = append(groups, int(child2.Data().(float64)))
			}
			item.Groups = groups
			return &item
		}
	}
	return nil
}

func (c *Catalog) Items(groupId int) []*Item {
	result := make([]*Item, 0)
	ids := make([]int, 0) // Item ids
	items, _ := c.data.S("items").Children()
	for _, child := range items {
		items2, _ := child.S("groups").Children()
		for _, child2 := range items2 {
			if groupId == int(child2.Data().(float64)) {
				ids = append(ids, int(child2.Data().(float64)))
			}
		}
	}
	// TODO: Clean ids from doubles?
	for _, id := range ids {
		item := c.Item(id)
		if item != nil {
			result = append(result, item)
		}
	}
	return result
}

func (c *Catalog) ItemsAll() []*Item {
	result := make([]*Item, 0)
	items, _ := c.data.S("items").Children()
	for _, child := range items {
		id := int(child.S("id").Data().(float64))
		item := c.Item(id)
		if item != nil {
			result = append(result, item)
		}
	}
	return result
}

func (c *Catalog) Dosages(itemId int) []string {
	dosages := make([]string, 0)
	packs, _ := c.data.S("packs").Children()
	for _, child := range packs {
		packItemId := int(child.S("item_id").Data().(float64))
		if itemId == packItemId {
			dosages = append(dosages, child.S("dosage").Data().(string))
		}
	}
	RemoveDuplicates(&dosages)
	return dosages
}

func (c *Catalog) Pack(packId int) *Pack {
	packs, _ := c.data.S("packs").Children()
	for _, child := range packs {
		id := int(child.S("id").Data().(float64))
		if id == packId {
			pack := Pack{}
			pack.Id = id
			pack.InStock = child.S("in_stock").Data().(bool)
			pack.Sale = child.S("sale").Data().(bool)
			saleAmount, _ := strconv.ParseFloat(child.S("sale_amount").Data().(string), 64)
			pack.SaleAmount = saleAmount
			pack.ItemId = int(child.S("item_id").Data().(float64))
			pack.UnitsQty = int(child.S("units_qty").Data().(float64))
			pack.UnitKind = child.S("unit_kind").Data().(string)
			retailPrice, _ := strconv.ParseFloat(child.S("retail_price").Data().(string), 64)
			pack.RetailPrice = retailPrice
			pack.Dosage = child.S("dosage").Data().(string)
			return &pack
		}
	}
	return nil
}

func (c *Catalog) Packs(itemId int) []*Pack {
	result := make(sortablePacks, 0)
	packIds := make([]int, 0)
	packs, _ := c.data.S("packs").Children()
	for _, child := range packs {
		packItemId := int(child.S("item_id").Data().(float64))
		if packItemId == itemId {
			packIds = append(packIds, int(child.S("id").Data().(float64)))
		}
	}
	for _, pid := range packIds {
		result = append(result, c.Pack(pid))
	}
	sort.Sort(result)

	return result
}

func (c *Catalog) PacksForDosage(itemId int, dosage string) []*Pack {
	result := make([]*Pack, 0)
	packs := c.Packs(itemId)
	for _, pack := range packs {
		if pack.Dosage == dosage {
			result = append(result, pack)
		}
	}
	return result
}

func (c *Catalog) MinUnitPricePack(itemId int) *Pack {
	var (
		pack     *Pack
		minPrice float64
	)
	packs := c.Packs(itemId)
	if len(packs) == 0 {
		return nil
	}
	pack = packs[0]
	minPrice = pack.RetailPrice / float64(pack.UnitsQty)
	for _, nextPack := range packs {
		price := nextPack.RetailPrice / float64(nextPack.UnitsQty)
		if price < minPrice {
			minPrice = price
			pack = nextPack
		}
	}
	return pack
}

func (c *Catalog) Assets(itemId int) []*Asset {
	results := make([]*Asset, 0)
	assets, _ := c.data.S("assets").Children()
	for _, a := range assets {
		id := int(a.S("item_id").Data().(float64))
		if id == itemId {
			asset := &Asset{Id: id}
			asset.ItemId = int(a.S("item_id").Data().(float64))
			asset.Kind = a.S("kind").Data().(string)
			asset.Url = a.S("url").Data().(string)
			results = append(results, asset)
		}
	}
	return results
}

func (c *Catalog) Group(groupId int) *Group {
	groups, _ := c.data.S("groups").Children()
	for _, g := range groups {
		if int(g.S("id").Data().(float64)) == groupId {
			name := g.S("name").Data().(string)
			return &Group{Id: groupId, Name: name, Slug: slug.Slug(name)}
		}
	}
	return nil
}

func (c *Catalog) Groups() []*Group {
	res := make([]*Group, 0)
	groups, _ := c.data.S("groups").Children()
	for _, g := range groups {
		id := int(g.S("id").Data().(float64))
		name := g.S("name").Data().(string)
		res = append(res, &Group{Id: id, Name: name, Slug: slug.Slug(name)})
	}
	return res
}

func (c *Catalog) Search(what string) []*Item {
	result := make([]*Item, 0)
	// We will find the substring in item name, item desc
	// TODO: add group name too
	if len(what) < 2 {
		return result
	}
	preparedWhat := strings.ToLower(what)
	for _, item := range c.ItemsAll() {
		if strings.Contains(strings.ToLower(item.Name), preparedWhat) ||
			strings.Contains(strings.ToLower(item.Desc), preparedWhat) {
			result = append(result, item)
		}
	}
	return result
}

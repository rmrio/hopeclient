package hopeclient

type ShippingMethod struct {
	Id            int
	FreeThreshold float64
	Price         float64
	Desc          string
	Name          string
}

type UnitKindMap map[string]string

type Group struct {
	Id   int
	Name string
	Slug string
}

type Item struct {
	Id     int
	Name   string
	Groups []int
	Desc   string
	Slug   string
}

type Asset struct {
	Id     int
	ItemId int
	Url    string
	Kind   string
}

type Pack struct {
	Id          int
	InStock     bool
	Sale        bool
	SaleAmount  float64
	ItemId      int
	UnitsQty    int
	UnitKind    string
	RetailPrice float64
	Dosage      string
}

type sortablePacks []*Pack

func (slice sortablePacks) Len() int {
	return len(slice)
}

func (slice sortablePacks) Less(i, j int) bool {
	return slice[i].RetailPrice < slice[j].RetailPrice
}

func (slice sortablePacks) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}
